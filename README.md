This research project is described in a blog post and associated BSides talk which can be found here: https://penconsultants.com/home/presentation-rats-without-borders/ 

* 1; JavaScript
* 2; Java
* 3; Python
* 4; CSS
* 5; PHP
* 6; Ruby
* 7; C/C++
* 8; Shell
* 9; C#
* 10; Objective-C
* 11; R
* 12; VimL  (aka Vimscript or Vim script)
* 13; Go
* 14; Perl
* 15; CoffeeScript
* 16; TeX
* 17; PowerShell
* 18; Visual Basic
* 19; Swift
* 20; ActionScript (AS3, AIR)
* 21; AppleScript
